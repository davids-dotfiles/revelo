<h1>revelo</h1> 

Unveil your student's names in your Microsoft Teams video meetings.

GitLab project location: https://gitlab.com/davids-dotfiles/revelo


<h3>SHORT DESCRIPTION</h3>

<p>Replaces useless names of your student's visible names in Microsoft Teams (e.g. "schoolname-8b") with recognizable names extracted from their User Principal Name (e.g. "J.Doe@school.city.de") in order to identify your students by name.</p>



<h3>INSTALL INSTRUCTIONS</h3>

Easiest way to use this script:
- Install the <a href=https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo>Tampermonkey</a> extension to your browser.
- Restart your browser.
- Click on this link https://gitlab.com/davids-dotfiles/revelo/-/raw/master/revelo.user.js
- Tampermonkey should open a tab and ask you, if you want to install / reinstall / update the script. Confirm after your own validation.

A chromium-based browser with blink-engine is required (e.g. Chrome, Edge, Opera, Brave, etc.). <br>
It does <b>not</b> work with webkit- or gecko-based browsers (e.g. <strike>Firefox, Safari, etc.</strike>)


<h3>HISTORY</h3>

<p>Some schools in my area use the city's Microsoft Teams server. The people who created the student accounts however did not think of teachers who would like to see the names of their students in video calls. Instead, the so called "TUN" (Teams User Name) is set to the name of their school + the grade year when the account was created (e.g. schoolname-8b). Obviously this naming pattern was also not that great because students tend to change grade years... usually once a year :P</p>

<p>There is <i>some</i> identifying information hidden in the application, because everyone signs in with their so called "UPN" (<i>User Principal Name</i> or <i>Benutzerprinizpalname</i>) to Microsoft services. We can make this data show up in a more visible place for the teacher.</p>


<h3>DISCLAIMER</h3>

- This script does NOT alter or modify any database. It does NOT access the internet nor saves any personal information.
- It only changes the way and order of the information that is being displayed on your browser LOCALLY.
- There is no additional personal information being revealed, other than what was already available and transfered by Microsoft to your device.
- You can see for yourself what this script does by looking at the source code <a href=https://gitlab.com/davids-dotfiles/revelo/-/blob/master/revelo.user.js>here</a>. It is licensed with the MIT license.
- I cannot guarantee that this script works on your browser without hiccups or is applicable to your school. Adjustments may be necessary for other institutions.
- As much as I can guarantee that this script does no harm, I cannot say this for other scripts that are installable with Tampermonkey. 
<h3>Beware of other scripts and don't install anything that you have not inspected yourself first! Never trust code that is unknown to you.<h3>
