// ==UserScript==
// @name         revelo
// @version      0.2
// @namespace    https://gitlab.com/davids-dotfiles/revelo
// @author       David Djaja
// @license      MIT License
// @description  Extracts UPN names (e.g. "J.Doe@school.city.de") found in a Microsoft Teams meeting, and uses them to replace visible names (e.g. "schoolname-8b").
//
//               The user names in the school I am teaching at do not bear the students' names, 
//               but have been assigned to their school's name and their class (e.g. schoolname-8b).
//               It has become a liability for educators, as they are not able to see any student names to identify them.
//               Their personal information (last names) is hidden in form of their "User Principal Names" (Benutzerprinizpalnamen), 
//               which are used by them to sign into Microsoft systems.
//               This script extracts the names of UPNs found in a video meeting, and uses them to replace the visible names in your browser.
//
// Disclaimer:   This script does not alter or modify any database. It does not access the internet nor saves any personal information.
//               It only swaps the way and order of the information that is being displayed on your browser LOCALLY.
//               There is no additional personal information being revealed, other than what was already transfered by Microsoft to your device.
//               You can see for yourself what this script does by looking at the (open) source code below.
//               I cannot guarantee that this script works on your browser without problems or is applicable to your school. 
//               Adjustments may be necessary for other institutions.
//
// @include      https://teams.microsoft.com/*
// @include      http://teams.microsoft.com/*
// @match        http*://teams.microsoft.com/*
// @require      http://code.jquery.com/jquery-3.5.1.js
// ==/UserScript==


function findNReplace() {

    // HTMLCollection of all elements with "ts-user-name", which contains a name like "gy-schoolname-6d"
    var tunCollection = document.getElementsByClassName("ts-user-name");
    // HTMLCollection of all elements with "ts-user-pictures" (which contain the UPNs as an attribute!!!)
    var upnCollection = document.getElementsByClassName("ts-user-picture");

    var currentUPN;

    // check if the collections are not empty
    if (tunCollection.length > 0) {
        for (var i=0; i < upnCollection.length; i++) {
            // Extract the UPN-attribute from the user-picture element.
            // Remove the email part ("@school.city.de") with a regular expression.
            currentUPN = upnCollection[i].getAttribute("upn").replace(/@.*/, '');
            // Replace the content of the TUN element with the value of the UPN
            tunCollection[i].innerHTML = currentUPN;
        }
    }
}

(function() {
    // repeat the function every 4 seconds in case of changes in the meeting
    setInterval(findNReplace, 4000);
})();
